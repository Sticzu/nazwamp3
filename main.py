#!/usr/bin/python3
import eyed3
import os
def renametotitle(filelocal):
    audiofile = eyed3.load(filelocal)
    tracknum = ""
    if audiofile.tag.track_num != None:
        if audiofile.tag.track_num[0] < 10:
            tracknum = "0" + str(audiofile.tag.track_num[0]) + ". "
        else:
             tracknum = str(audiofile.tag.track_num[0]) + ". "
    else:
        print ("brak numeru utworu", end = '')

    nazwakoncowa = tracknum + audiofile.tag.title + ".mp3"
    if audiofile.tag.title != None:
        os.rename(filelocal, nazwakoncowa)
        print(nazwakoncowa)

iteration = 0
files = os.listdir(".")
for file in files:
    #print (list, " :: ", file)

    try:
        renametotitle( file )
        iteration += 1
    except:
        print("FAIL: ", file)

print("liczba zminionych plików: ", iteration)
